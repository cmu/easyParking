package com.googleMap;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This class echoes a string called from JavaScript.
 */
public class EPGoogle extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("echo")) {
            String message = args.getString(0);
            String start= args.getJSONObject(0).getString("start");
            String end= args.getJSONObject(0).getString("end");

            //插入调用google地图的函数
            Intent intent = new Intent(Intent.ACTION_VIEW);

            // String url = "http://maps.google.com/maps?saddr=32.123058,118.907434(金港)&daddr=32.0856089,118.7946453(南京火车站)";
            String url = "http://maps.google.com/maps?saddr="+start+"&daddr="+end+"\"";

            Uri uri = Uri.parse(url);
            intent.setData(uri);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK & Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
            try {
                this.cordova.getActivity().startActivity(intent);
            } catch (ActivityNotFoundException e) {
                Toast.makeText( this.cordova.getActivity(), "还未安装Google Maps", Toast.LENGTH_SHORT).show();
            }


            //this.echo(message, callbackContext);
            return true;
        }
        return false;
    }

    private void echo(String message, CallbackContext callbackContext) {
        if (message != null && message.length() > 0) {
            callbackContext.success(message);
        } else {
            callbackContext.error("Expected one non-empty string argument.");
        }
    }
}
