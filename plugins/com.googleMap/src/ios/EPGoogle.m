/********* EPGoogle.m Cordova Plugin Implementation *******/

#import <Cordova/CDV.h>

@interface EPGoogle : CDVPlugin {
  // Member variables go here.
}

- (void)echo:(CDVInvokedUrlCommand*)command;
@end

@implementation EPGoogle

- (void)echo:(CDVInvokedUrlCommand*)command
{
    NSLog(@"Cordova iOS plugin test~~~~~~~~~~~~~~~~~~~~~~");
    CDVPluginResult* pluginResult = nil;
    NSString* echo = [command.arguments objectAtIndex:0];
    NSString* url1=@"comgooglemaps-x-callback://?saddr=";
    NSString* url2=@"&daddr=";
    
    
    NSString* start=[[command.arguments objectAtIndex:0] objectForKey:@"start"];
    NSString* end=[[command.arguments objectAtIndex:0] objectForKey:@"end"];
    NSLog(@"%@",start);
    NSLog(@"%@",end);
    
    NSString* url = [url1  stringByAppendingFormat : @"%@%@%@" ,start,url2,end];
    
    NSLog(@"%@",url);
    
    NSURL *testURL = [NSURL URLWithString:@"comgooglemaps-x-callback://"];
    if ([[UIApplication sharedApplication] canOpenURL:testURL]) {
//        UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"标题" message:@"开始导航！" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
//        [alertview show];
        NSString *directionsRequest =url;
        NSURL *directionsURL = [NSURL URLWithString:directionsRequest];
        [[UIApplication sharedApplication] openURL:directionsURL];
    } else {
        UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"标题" message:@"请先安装Google Maps！" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        [alertview show];
        NSLog(@"Can't use comgooglemaps-x-callback:// on this device.");
    }

    if (echo != nil && [echo length] > 0) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:echo];
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }

    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

@end
