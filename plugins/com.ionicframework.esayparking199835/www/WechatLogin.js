var exec = require('cordova/exec');

var WeChatLogin = function(arg0, success, error) {
    exec(success, error, "WechatLogin", "Login_Wechat", [arg0]);
};

window.plugins=window.plugins||{};
window.plugins.Login_Wechat=WeChatLogin;
exports.Login_Wechat=WeChatLogin;