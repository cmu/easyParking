/**
 * Created by luxiaohui on 16/3/22.
 */
angular.module('epApp')
  .config(function ($urlRouterProvider) {
    var otherwisePath = 'app/home';
    $urlRouterProvider.otherwise(otherwisePath);

    console.log("----路由顺序----999：unknown router directly go to " + otherwisePath);
  });
