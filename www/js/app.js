/**
 * easyparking app
 */
angular.module('epApp', [
    'ionic',
    'epApp.controllers',
    'epApp.routers',
    'epApp.services',
    'epApp.directive',
    'ngCordova'
  ])
  .config(function ($httpProvider, $stateProvider, $urlRouterProvider, $ionicConfigProvider) {
    //set ios android style
    $ionicConfigProvider.platform.ios.tabs.style('standard');
    $ionicConfigProvider.platform.ios.tabs.position('bottom');
    $ionicConfigProvider.platform.android.tabs.style('standard');
    $ionicConfigProvider.platform.android.tabs.position('bottom');

    $ionicConfigProvider.platform.ios.navBar.alignTitle('center');
    $ionicConfigProvider.platform.android.navBar.alignTitle('center');

    $ionicConfigProvider.platform.ios.backButton.previousTitleText(' ').icon('ion-ios-arrow-thin-left');
    $ionicConfigProvider.platform.android.backButton.previousTitleText(' ').icon('ion-android-arrow-back');

    $ionicConfigProvider.platform.ios.views.transition('ios');
    $ionicConfigProvider.platform.android.views.transition('android');


  })

  .controller('NaviCtrl', function ($rootScope, $ionicPopup, $q) {
    console.log("初始化导航控制器");
    //util part
    //提示框
    var showAlert = function (myMsg) {
      var msg = "", callback;
      if (myMsg && typeof (myMsg) == 'object' && myMsg.msg && myMsg.callback) {
        msg = myMsg.msg;
        callback = myMsg.callback;
      } else {
        msg = myMsg;
        callback = null;
      }
      var alertPopup = $ionicPopup.alert({
        title: '提示',
        template: msg,
        okText: '确定'

      });
      alertPopup.then(function (res) {
        if (callback && typeof (callback) == 'function') {
          callback();
        }
      });
    };
    $rootScope.showAlert = showAlert;


    // 是否为空是否定义
    var isNullOrUndefined = function (obj) {
      return (obj === null || obj === undefined);
    };
    $rootScope.isNullOrUndefined = isNullOrUndefined;

    $rootScope.showToastOrDialog = function (msg, success) {
      if (isNullOrUndefined(window.plugins) || isNullOrUndefined(window.plugins.toast)) {
        showAlert({msg: msg, callback: success});
      } else {
        if (success instanceof Function)
          window.plugins.toast.showShortCenter(msg, success, function () {
          });
        else {
          window.plugins.toast.showShortCenter(msg,
            function () {
            },
            function () {
            }
          );
        }
      }
    };

    /**
     * 提示确认框
     * @param config {title,content}
     * @returns {*|promise} promise ok cancel
     */
    $rootScope.showConfirm = function (config) {
      var deferred = $q.defer();
      config = config || {};
      config.title = config.title || '提示';
      config.content = config.content || '';
      var confirmPopup = $ionicPopup.confirm({
        title: '<strong>' + config.title + '</strong>',
        template: config.content,
        okText: '确认',
        cancelText: '取消'
      });

      confirmPopup.then(function (res) {
        if (res) {
          deferred.resolve();
        } else {
          deferred.reject();
        }

      });
      return deferred.promise;
    };
  })

  .run(function ($ionicPlatform, $ionicPopup, $ionicHistory, $location) {

    //主页面显示退出提示框
    $ionicPlatform.registerBackButtonAction(function (e) {
      e.preventDefault();

      function showConfirm() {
        var confirmPopup = $ionicPopup.confirm({
          title: '<strong>退出应用?</strong>',
          template: '你确定要退出应用吗?',
          okText: '退出',
          cancelText: '取消'
        });

        confirmPopup.then(function (res) {
          if (res) {
            ionic.Platform.exitApp();
          } else {
            // Don't close
          }
        });
      }

      var vh = $ionicHistory.viewHistory();
      // Is there a page to go back to?
      if ($location.path() == '/main' || $location.path() == '/login') {
        showConfirm();
      } else if (vh.backView) {
        console.log('currentView:', vh.currentView);
        // Go back in history
        $ionicHistory.goBack();
      } else {
        // This is the last page: Show confirmation popup
        showConfirm();
      }

      return false;
    }, 101);
    $ionicPlatform.ready(function () {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      }
      if (window.StatusBar) {
        StatusBar.styleDefault();
      }
      console.log("ionic ready");
      if (typeof String.prototype.startsWith != 'function') {
        String.prototype.startsWith = function (prefix) {
          return this.slice(0, prefix.length) === prefix;
        };
      }


    });
  });

angular.module('epApp.controllers', ['ui.router','ionic', 'ionic-datepicker']);
angular.module('epApp.routers', ['ui.router']);
angular.module('epApp.services', ['ui.router']);
angular.module('epApp.directive', ['ui.router']);

