angular.module('epApp.services')
  .factory('SearchHist', function ($http, $q) {
    return {
      fdetail:fdetail,
      saveSearch:saveSearch,
      saveParkSearch:saveParkSearch,
      savePOISearch:savePOISearch,
      searchHist:searchHist,
      getUserInfo:getUserInfo,
      getUserByID:getUserByID,
      getParkByID:getParkByID,
      getCollectOrNot:getCollectOrNot,
      deleteCollect:deleteCollect,
      insertCollect:insertCollect,
      fPraise:fPraise,
      AddPraise:AddPraise,
      LatestCom:LatestCom,
      PCom:PCom,
      fmark:fmark,
      getOffParkCharge:getOffParkCharge,
      getOnParkCharge:getOnParkCharge,
      getOnParkSpace:getOnParkSpace,
      getOffParkEntity:getOffParkEntity,
      getDifficulty:getDifficulty,
    }

    //获取附近停车场
    function fdetail(lat,lng) {
      var url = 'http://139.162.22.37:3000/Parkdetails/Parkdetail?lat='+lat+'&lng='+lng;
      var defer = $q.defer();
      $http.get(url, {})
        .then(
          //success
          function (data) {
            defer.resolve(data);
          },
          //error
          function (error) {
            defer.reject(error);
          }
        );
      return defer.promise;
    };

    ///保存地址搜索记录
    function saveSearch(user_id,search_value) {
      var url = 'http://139.162.22.37:3000/searchHistory/saveSearch?user_id='+user_id+'&search_value='+search_value+'';
      var defer = $q.defer();
      $http.get(url, {})
        .then(
          //success
          function (data) {
            defer.resolve(data);
          },
          //error
          function (error) {
            defer.reject(error);
          }
        );
      return defer.promise;
    };

    ///保存停车场搜索记录
    function saveParkSearch(user_id,search_value) {
      //var url = 'http://139.162.22.37:3000/users/login?userType='+type+'&userName='+userName+'&userPwd='+userPwd+'';

      var url = 'http://139.162.22.37:3000/searchHistory/saveParkSearch?user_id='+user_id+'&search_value='+search_value;
      var defer = $q.defer();
      $http.get(url, {})
        .then(
          //success
          function (data) {
            defer.resolve(data);
          },
          //error
          function (error) {
            defer.reject(error);
          }
        );
      return defer.promise;
    };

    ///保存POI搜索记录
    function savePOISearch(user_id,search_value) {
      //var url = 'http://139.162.22.37:3000/users/login?userType='+type+'&userName='+userName+'&userPwd='+userPwd+'';

      var url = 'http://139.162.22.37:3000/searchHistory/savePOISearch?user_id='+user_id+'&search_value='+search_value;
      var defer = $q.defer();
      $http.get(url, {})
        .then(
          //success
          function (data) {
            defer.resolve(data);
          },
          //error
          function (error) {
            defer.reject(error);
          }
        );
      return defer.promise;
    };

    ///获取用户搜索、导航历史记录
    function searchHist(user_id) {
      //var url = 'http://139.162.22.37:3000/users/login?userType='+type+'&userName='+userName+'&userPwd='+userPwd+'';
      var url = 'http://139.162.22.37:3000/searchHistory/search?user_id=%s'.format(user_id);
      var defer = $q.defer();
      $http.get(url, {})
        .then(
          //success
          function (data) {
            defer.resolve(data);
          },
          //error
          function (error) {
            defer.reject(error);
          }
        );
      return defer.promise;
    }

    //获取用户信息
    function getUserInfo(user_email){
      var url = 'http://139.162.22.37:3000/users/queryByEmail?userEmail=%s'.format(user_email);
      var defer = $q.defer();
      $http.get(url, {})
        .then(
          //success
          function (data) {
            defer.resolve(data);
          },
          //error
          function (error) {
            defer.reject(error);
          }
        );
      return defer.promise;
    }

    //根据user_id获取用户信息
    function getUserByID(user_id){
      var url = 'http://139.162.22.37:3000/users/queryById?user_id='+user_id;
      var defer = $q.defer();
      $http.get(url, {})
        .then(
          //success
          function (data) {
            defer.resolve(data);
          },
          //error
          function (error) {
            defer.reject(error);
          }
        );
      return defer.promise;
    }

    function getParkByID(park_id){
      var url='http://139.162.22.37:3000/Parkdescs/queryPark?parkID='+park_id;
      var defer = $q.defer();
      $http.get(url, {})
        .then(
          //success
          function (data) {
            defer.resolve(data);
          },
          //error
          function (error) {
            defer.reject(error);
          }
        );
      return defer.promise;
    }

    function getCollectOrNot(user_id,search_value,search_type){
      var url = 'http://139.162.22.37:3000/users/queryCollectOrNot?user_id='+user_id+'&search_value='+search_value+'&search_type='+search_type;
      var defer = $q.defer();
      $http.get(url, {})
        .then(
          //success
          function (data) {
            defer.resolve(data);
          },
          //error
          function (error) {
            defer.reject(error);
          }
        );
      return defer.promise;
    }

    function deleteCollect(user_id,search_value,search_type){
      var url = 'http://139.162.22.37:3000/users/deleteCollect?user_id='+user_id+'&search_value='+search_value+'&search_type='+search_type;
      var defer = $q.defer();
      $http.get(url, {})
        .then(
          //success
          function (data) {
            defer.resolve(data);
          },
          //error
          function (error) {
            defer.reject(error);
          }
        );
      return defer.promise;
    }

    function insertCollect(user_id,search_value,search_type){
      var url = 'http://139.162.22.37:3000/users/insertCollect?user_id='+user_id+'&search_value='+search_value+'&search_type='+search_type;
      var defer = $q.defer();
      $http.get(url, {})
        .then(
          //success
          function (data) {
            defer.resolve(data);
          },
          //error
          function (error) {
            defer.reject(error);
          }
        );
      return defer.promise;
    }
    //获取当前停车场的热门评论
    function fPraise(park_id) {
      var url = 'http://139.162.22.37:3000/PComments/PComment?park_id=%s'.format(park_id);
      var defer = $q.defer();
      $http.get(url, {})
        .then(
          //success
          function (data) {
            defer.resolve(data);
          },
          //error
          function (error) {
            defer.reject(error);
          }
        );
      return defer.promise;
    }

    //增加点赞数目
    function AddPraise(user_id,park_id){
      var url = 'http://139.162.22.37:3000/PComments/PComMark?user_id='+user_id+'&park_id='+park_id;
      var defer = $q.defer();
      $http.get(url, {})
        .then(
          //success
          function (data) {
            defer.resolve(data);
          },
          //error
          function (error) {
            defer.reject(error);
          }
        );
      return defer.promise;
    }

    //获取当前停车场的最新评论
    function LatestCom(park_id) {
      var url = 'http://139.162.22.37:3000/PComments/PLComment?park_id=%s'.format(park_id);
      var defer = $q.defer();
      $http.get(url, {})
        .then(
          //success
          function (data) {
            defer.resolve(data);
          },
          //error
          function (error) {
            defer.reject(error);
          }
        );
      return defer.promise;
    }

    function PCom(user_id,park_id,context,mark,time,user_nickname) {
      var url = 'http://139.162.22.37:3000/parkCom/savePCom?user_id='+user_id+'&park_id='+park_id+'&context='+context+'&mark='+mark+'&time='+time+'&user_nickname='+user_nickname;
      var defer = $q.defer();
      $http.get(url, {})
        .then(
          //success
          function (data) {
            defer.resolve(data);
          },
          //error
          function (error) {
            defer.reject(error);
          }
        );
      return defer.promise;
    };

    //获取当前停车场的评分
    function fmark(park_id) {
      var url = 'http://139.162.22.37:3000/Parkdescs/Parkdescs?park_id='+park_id;
      var defer = $q.defer();
      $http.get(url, {})
        .then(
          //success
          function (data) {
            defer.resolve(data);
          },
          //error
          function (error) {
            defer.reject(error);
          }
        );
      return defer.promise;
    };

    //获取一般停车场的收费
    function getOffParkCharge(park_id) {
      var url = 'http://139.162.22.37:3000/Parkdescs/offStreetCharge?park_id='+park_id;
      var defer = $q.defer();
      $http.get(url, {})
        .then(
          //success
          function (data) {
            defer.resolve(data);
          },
          //error
          function (error) {
            defer.reject(error);
          }
        );
      return defer.promise;
    };
    //获取街边停车场的收费
    function getOnParkCharge(park_id) {
      var url = 'http://139.162.22.37:3000/Parkdescs/onStreetCharge?park_id='+park_id;
      var defer = $q.defer();
      $http.get(url, {})
        .then(
          //success
          function (data) {
            defer.resolve(data);
          },
          //error
          function (error) {
            defer.reject(error);
          }
        );
      return defer.promise;
    };

    //获取街边停车场的停车位
    function getOnParkSpace(park_id) {
      var url = 'http://139.162.22.37:3000/Parkdescs/ParkSpace?park_id='+park_id;
      var defer = $q.defer();
      $http.get(url, {})
        .then(
          //success
          function (data) {
            defer.resolve(data);
          },
          //error
          function (error) {
            defer.reject(error);
          }
        );
      return defer.promise;
    };

    //获取非街边停车场的实体
    function getOffParkEntity(park_id) {
      var url = 'http://139.162.22.37:3000/Parkdescs/ParkEntity?park_id='+park_id;
      var defer = $q.defer();
      $http.get(url, {})
        .then(
          //success
          function (data) {
            defer.resolve(data);
          },
          //error
          function (error) {
            defer.reject(error);
          }
        );
      return defer.promise;
    };

    //获取停车场的动态信息
    function getDifficulty(park_id) {
      var url = 'http://139.162.22.37:3000/Parkdescs/ParkRealtime?park_id='+park_id;
      var defer = $q.defer();
      $http.get(url, {})
        .then(
          //success
          function (data) {
            defer.resolve(data);
          },
          //error
          function (error) {
            defer.reject(error);
          }
        );
      return defer.promise;
    };
  });
