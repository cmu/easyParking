/**
 * Created by luxiaohui on 16/3/22.
 */
angular.module('epApp.routers')
  .config(function ($stateProvider) {

    $stateProvider.state('app.home', {
      cache: false,
      url: '/home?userName&loginType',
      views: {
        'homeContent': {
          templateUrl: 'templates/home/home.html',
          controller: 'HomeCtrl'

        }
      }
    });

    console.log('----路由顺序----2：Home router');
  });
