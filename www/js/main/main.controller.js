/**
 * Created by luxiaohui on 16/3/22.
 */
angular.module('epApp.controllers')
  .controller('AppCtrl', function ($scope,$ionicHistory) {
    console.log("进入App");
    $scope.$on('$ionicView.enter', function () {
      //从登陆页进入main 后，把跳转历史清空
      console.log('rootView');
      $ionicHistory.clearHistory();
    });
  });
