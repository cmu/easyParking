/**
 * Created by luxiaohui on 16/3/22.
 */
angular.module('epApp.routers')
  .config(function ($stateProvider) {

    $stateProvider
      .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/main.html',
        controller: 'AppCtrl'
      });
    console.log('----路由顺序----1：App router');
  });
