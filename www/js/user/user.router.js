/**
 * Created by luxiaohui on 16/3/22.
 */
angular.module('epApp.routers')
  .config(function ($stateProvider) {
//$state.go("login")
    $stateProvider.state('login', {
      url: '/login',
      templateUrl: 'templates/user/login.html',
      controller: 'UserCtrl'
    });

    $stateProvider.state('forgetpw', {
      url: '/forgetpw',
      templateUrl: 'templates/user/forgetpw.html',
      controller: 'forgetpwd'
    });


    $stateProvider.state('userset', {
      url: '/userset',
      templateUrl: 'templates/user/userset.html',
      controller: 'userset'
    });

    //$state.go("register")
    $stateProvider.state('register', {
      url: '/register?userName',
      templateUrl: 'templates/user/register.html',
      controller: 'RegController'
    });


    console.log('----路由顺序----3：User router');


  });
