
angular.module('epApp.controllers')
  .controller('UserCtrl', function ($scope, $rootScope, $timeout, $ionicLoading, $location, $state, User) {

    $scope.user = {};
    $scope.fgtpw=fgtpw;
    $scope.login=login;
    $scope.register=register;
    $scope.goHome=goHome;
    $scope.login=login;

    $scope.wechat_login=function(){
      window.plugins.Login_Wechat("",
        function(success){
          //NickName
          //UnionID
          //alert("nikename:"+success.NickName);
          //用户的微信唯一标识码
          User.wechatLogin(success.NickName, success.UnionID)
            .then(function (data) {
              $location.url('/app/home?userName='+success.UnionID+'&loginType='+'Wechat');
              document.getElementById('userItem').style.display='block';
              document.getElementById('user').innerHTML=success.NickName;
              document.getElementById('login').innerHTML='注销账号';
            }, function (error) {
              //console.log(error);
              //$rootScope.showToastOrDialog(error.data.msg, function () {
              //
              //});
            })
            .finally(function () {
              $ionicLoading.hide();
            });
        },
        function(error){
          $rootScope.showToastOrDialog('登录失败，请重试', function () {
          });
        }
      );
      //$scope.$apply();
    };

    $scope.userLogin=function(){
      if($scope.user.name){//已有用户登录

      }else{//登陆
        $location.path('/login');
      }
    };

     function fgtpw() {
       $location.path('/forgetpw');
     }


    function login() {
      $ionicLoading.show({
        template: '正在登陆中...'
      });

      //console.log(type);
      User.login($scope.user.name, $scope.user.pwd)
        .then(function (data) {
          $location.url('/app/home?userName='+$scope.user.name+'&loginType='+'Email');
        }, function (error) {
          console.log(error);
          $rootScope.showToastOrDialog(error.data.msg, function () {

          });
        })
        .finally(function () {
          $ionicLoading.hide();
        });
    }

    function register() {

      $location.url('/register?userName='+$scope.user.name);

    }


    function goHome(){
      $location.path('/app/home');

    }



    console.log("进入登陆页");
  });

