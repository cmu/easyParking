/**
 * Created by luxiaohui on 16/3/22.
 */
angular.module('epApp.services')
  .factory('User', function ($http, $q) {
      return {
        login: login,
        wechatLogin:wechatLogin,

      };


      function login(userName, userPwd) {
        //var url = 'http://139.162.22.37:3000/users/login?userType='+type+'&userName='+userName+'&userPwd='+userPwd+'';

        var url = 'http://139.162.22.37:3000/users/login?userName=%s&userPwd=%s'.format(userName, userPwd);
        var defer = $q.defer();
        $http.get(url, {})
          .then(
            //success
            function (data) {
              defer.resolve(data);
            },
            //error
            function (error) {
              defer.reject(error);
            }
          );
        return defer.promise;
      }

    function wechatLogin(nick_name,wechat_unionid){
      var url='http://139.162.22.37:3000/users/queryByWechat?nick_name='+nick_name+'&wechat_unionid='+wechat_unionid;
      var defer = $q.defer();
      $http.get(url, {})
        .then(
          //success
          function (data) {
            defer.resolve(data);
          },
          //error
          function (error) {
            defer.reject(error);
          }
        );
      return defer.promise;
    }
    }
  );
